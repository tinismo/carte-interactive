<?php
/**
 * Created by PhpStorm.
 * User: Tinismo
 * Date: 23/09/2018
 * Time: 02:52
 */

$dbh = new PDO('mysql:host=localhost;dbname=bd_ppe3_veliberte;charset=utf8', 'root', '');

$sql = "SELECT * FROM velo";
$stmt = $dbh->prepare($sql);
$stmt->execute();
$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

$retour = json_encode($results);

echo $retour;

?>